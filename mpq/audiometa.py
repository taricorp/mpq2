import json, logging, os, subprocess

__all__ = ['AudioMeta']

log = logging.getLogger('mpq.audiometa')

FFPROBE_OPTS = "ffprobe -of json=compact=1 -show_entries format_tags=title,artist,album".split()

def ffprobeMeta(file):
    # TODO broken.
    with open(os.devnull, 'w') as devnull:
        p = subprocess.Popen(FFPROBE_OPTS + [file], stdout=subprocess.PIPE, stderr=devnull)
        try:
            (stdout, _) = p.communicate(timeout=10)
        except subprocess.TimeoutExpired:
            p.kill()
            (stdout, _) = p.communicate()
        if p.returncode:
            raise Exception("ffprobe returned nonzero (%i)" %p.returncode)
        return json.loads(stdout.decode('utf-8'))['format']['tags']

def mutagenMeta(file):
    mm = mutagen.File(file, easy=True)
    out = {}
    for tag in ('artist', 'album', 'title'):
        if tag in mm:
            out[tag] = unicode(mm[tag][0])
        else:
            if tag == 'title':
                # Stupid hack, get the filename.
                out[tag] = file.rpartition('/')[2]
            else:
                out[tag] = None
    return out

try:
    import mutagen
    AudioMeta = mutagenMeta
except ImportError:
    AudioMeta = ffprobeMeta

if __name__ == '__main__':
    print('Using {0} prober'.format(AudioMeta.__name__))
    import sys
    for f in sys.argv[1:]:
        print('{0}:\n{1}'.format(f, AudioMeta(f)))
