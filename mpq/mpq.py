from twisted.python import log
from twisted.internet import reactor, threads
from twisted.web.resource import Resource
from twisted.web.server import Site, NOT_DONE_YET
from twisted.web.static import File

from autobahn.twisted.websocket import listenWS
from autobahn.wamp import WampServerFactory, WampServerProtocol

import cgi, json, os, shutil
import db
from audiometa import AudioMeta

class VoteServer(WampServerProtocol):
    def onSessionOpen(self):
        self.registerForPubSub('http://mpq.taricorp.net/vote')
        self.registerForPubSub('http://mpq.taricorp.net/newtrack')

class TrackUploadResource(Resource):
    isLeaf = True

    def __init__(self, musicdir, wsFactory):
        self.musicdir = musicdir
        self.wsFactory = wsFactory
        self.seqnum = 0

    def render_GET(self, request):
        return '''<html>
<form method="POST" enctype="multipart/form-data">
    <input name="file" type="file" />
    <input type="submit" value="submit"/>
</form>
</html>'''

    def processFileUpload(self, request):
        headers = request.getAllHeaders()
        form = cgi.FieldStorage(fp = request.content,
                                headers = headers,
                                environ = {'REQUEST_METHOD': 'POST',
                                           'CONTENT_TYPE': headers['content-type']})
        uploaded = form['file']
        # Strip filename to 32 chars, preserving extension
        filename = uploaded.filename
        head, _, ext = filename.rpartition('.')
        filename = '.'.join((head[:32-len(ext)], ext))
        filepath = os.path.join(self.musicdir, filename)

        # Write the uploaded file
        if os.path.exists(filepath):
            raise Exception("File of this name already exists")
        s = None
        try:
            with open(filepath, 'wb') as f:
                shutil.copyfileobj(uploaded.file, f)

            meta = AudioMeta(filepath)
            track = db.Track(filename=filename, title=meta['title'],
                             artist=meta['artist'], album=meta['album'])
            s = db.Session()
            s.add(track)
            s.commit()
        except:
            # Something failed. Clean up and rethrow.
            if os.path.exists(filepath):
                os.unlink(filepath)
            if s:
                s.rollback()
            raise
        
        return (track, request)

    def uploadComplete(self, args):
        # deferToThread ensures we're back on the reactor thread here
        track, request = args
        out = {}
        for prop in ('id', 'artist', 'album', 'title'):
            out[prop] = getattr(track, prop)
        json.dump(out, request)
        request.finish()
        self.wsFactory.dispatch("http://mpq.taricorp.net/newtrack", out)

    def render_POST(self, request):
        d = threads.deferToThread(self.processFileUpload, request)
        d.addCallback(self.uploadComplete)
        return NOT_DONE_YET

# Client does an HTTP upload, that does a factory.dispatch(...)
if __name__ == '__main__':
    import sys
    log.startLogging(sys.stdout)
    debug = len(sys.argv) > 1 and sys.argv[1] == 'debug'

    factory = WampServerFactory("ws://localhost:8001", debugWamp=debug)
    factory.protocol = VoteServer
    factory.setProtocolOptions(allowHixie76=True)
    listenWS(factory)

    webdir = File(".")
    webdir.putChild('upload', TrackUploadResource("./music/", factory))
    reactor.listenTCP(8000, Site(webdir))

    reactor.run()
