from twisted.internet import reactor
from autobahn.wamp import WampServerFactory, WampServerProtocol

# Laziness and I like decorators
def inReactorThread(f):
    def _(*args, **kwargs):
        reactor.callInThread(f, *args, **kwargs)
    return _

class ModificationProtocol(WampServerProtocol):
    # TODO deny newtrack publish from anything but the server
    def onSessionOpen(self):
        self.registerForPubSub('http://mpq.taricorp.net/vote')
        self.registerForPubSub('http://mpq.taricorp.net/newtrack')
        self.registerForPubSub('http://mpq.taricorp.net/playing')

class ModificationFactory(WampServerFactory):
    protocol = ModificationProtocol

    def __init__(self, iface, port, debug=False):
        uri = "ws://{0}:{1}".format(iface, port)
        WampServerFactory.__init__(self, uri, debug=debug)

    @inReactorThread
    def trackChanged_callback(self, tid):
        # This can fire when the reactor isn't yet running (before the reactor
        # starts but after the player is started), so wait until it is running.
        reactor.callWhenRunning(self.dispatch, 'http://mpq.taricorp.net/playing', tid)

def getFactory(iface, port, debug=False):
    factory = ModificationFactory(iface, port, debug)
    # https:/github.com/tavendo/AutobahnPython/issues/133
    factory.startFactory()
    return factory
