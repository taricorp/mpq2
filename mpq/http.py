import cgi, json, os, shutil
from base64 import b32encode
import struct, time

from twisted.internet import threads
from twisted.protocols.basic import FileSender
from twisted.python import log
from twisted.web.resource import Resource
from twisted.web.server import Site, NOT_DONE_YET
from twisted.web.static import File

from autobahn.twisted.resource import WebSocketResource

from . import audiometa, db

class TrackUploadResource(Resource):
    def __init__(self, musicdir):
        Resource.__init__(self)
        self.musicdir = musicdir
        self.seqnum = 0

    def createUniqueFile(self, basename):
        name, _, ext = basename.rpartition('.')
        f = None

        while f is None:
            # Base32 with padding stripped is guaranteed safe and fairly compact
            suffix = b32encode(struct.pack('!d', time.time())).rstrip('=')
            # Max 32, -2 dots and non-negotiable trailing bits
            trial_name = basename[:32 - 2 - len(suffix) - len(ext)]
            trial_name = '.'.join((trial_name, suffix, ext))
            trial_fullpath = os.path.join(self.musicdir, trial_name)
            try:
                # TODO needs O_BINARY on windows. Do we care?
                f = os.open(trial_fullpath, os.O_CREAT | os.O_EXCL | os.O_WRONLY, 0o666)
            except OSError as e:
                if e.errno != errno.EEXIST:
                    raise
        return (os.fdopen(f, 'wb'), trial_name)

    def processFileUpload(self, request):
        headers = request.getAllHeaders()
        form = cgi.FieldStorage(fp = request.content,
                                headers = headers,
                                environ = {'REQUEST_METHOD': 'POST',
                                           'CONTENT_TYPE': headers['content-type']})
        uploaded = form['file']
        # Strip filename to 32 chars, preserving extension
        # TODO do we have a guaranteed encoding?
        filename = unicode(uploaded.filename, 'utf-8', 'replace')

        s = None
        try:
            f, filename = self.createUniqueFile(filename)
            filepath = os.path.join(self.musicdir, filename)
            with f:
                shutil.copyfileobj(uploaded.file, f)

            meta = audiometa.AudioMeta(filepath)
            track = db.Track(filename=filename, title=meta['title'],
                             artist=meta['artist'], album=meta['album'])
            s = db.Session()
            s.add(track)
            s.commit()
        except:
            # Something failed. Clean up and rethrow.
            if os.path.exists(filepath):
                os.unlink(filepath)
            if s:
                s.rollback()
            raise
        
        return track.asCanonical()

    def uploadComplete(self, track, request):
        pass

    def render_POST(self, request):
        d = threads.deferToThread(self.processFileUpload, request)
        d.addCallback(self.uploadComplete, request)
        # TODO handle errors (errback)
        return NOT_DONE_YET

class TrackEnumerationResource(Resource):
    def enumerateTracks(self, request):
        out = []
        s = db.Session()
        tracks = [track.asCanonical() for track in s.query(db.Track)]
        return tracks

    def enumerationComplete(self, tracks, request):
        pass

    def render_GET(self, request):
        # TODO limit response size (could be a huge list)
        d = threads.deferToThread(self.enumerateTracks, request)
        d.addCallback(self.enumerationComplete, request)
        return NOT_DONE_YET

class ElectionFraudException(Exception): pass

class TrackResource(Resource):
    isLeaf = True

    def __init__(self, tid, wsFactory):
        Resource.__init__(self)
        self.tid = tid
        self.wsFactory = wsFactory

    def doVote(self, uid):
        s = db.Session()
        # Acquire lock on track
        track = s.query(db.Track). \
                  filter(db.Track.id == self.tid). \
                  with_lockmode('update'). \
                  one()
        # Check if vote allowed
        if s.query(db.Vote).get((uid, self.tid)) != None:
            raise ElectionFraudException('Already voted for track %i' %self.tid)

        track.score = db.Track.score + 1
        s.add(db.Vote(uid=uid, track=track))
        s.commit()
        return track

    def voteSuccessful(self, track, request):
        self.wsFactory.dispatch('http://mpq.taricorp.net/vote', track.id)
        request.write(str(track.score))
        request.finish()

    def voteDenied(self, failure, request):
        failure.trap(ElectionFraudException)
        # Too Many Requests (already voted)
        request.setResponseCode(429)
        request.write(failure.getErrorMessage())
        request.finish()

    def render_PUT(self, request):
        d = threads.deferToThread(self.doVote, request.getSession().uid)
        d.addCallbacks(self.voteSuccessful, self.voteDenied,
                       callbackArgs=(request,),
                       errbackArgs=(request,))
        return NOT_DONE_YET

    def render_GET(self, request):
        # TODO Should do the usual track.asCanonical
        pass

class AJAXTracksResource(TrackUploadResource, TrackEnumerationResource):
    def __init__(self, musicdir, wsFactory):
        TrackUploadResource.__init__(self, musicdir)
        self.wsFactory = wsFactory

    def uploadComplete(self, track, request):
        json.dump(track, request)
        request.finish()
        self.wsFactory.dispatch("http://mpq.taricorp.net/newtrack", track)

    def enumerationComplete(self, tracks, request):
        json.dump(tracks, request)
        request.finish()

    def getChild(self, tid, request):
        return TrackResource(int(tid), self.wsFactory)

class NowPlayingResource(Resource):
    def __init__(self, player):
        self.player = player

    def render_GET(self, request):
        elapsed, total = self.player.queryProgress()
        if elapsed != None and total != None:
            elapsed /= 1000000000
            total /= 1000000000
        out = {
            'id': self.player.nowPlayingID,
            'elapsed': elapsed,
            'length': total
        }
        return json.dumps(out)

class AudioStreamResource(Resource):
    def __init__(self, player):
        self.player = player

    def render_GET(self, request):
        # TODO support shoutcast semantics (Icy-Metadata header)
        # http://www.smackfu.com/stuff/programming/shoutcast.html
        # (Implement with a transform function on a FileSender-like object?)
        # Note that FileSender tries to read big chunks and appears to deadlock
        # when given a pipe.
        #
        # Twisted does a bunch of things that aren't useful here (sets chunked
        # encoding, assumes a content-length in some cases..), so write the
        # response headers on our own.
        transport = request.transport
        transport.writeSequence(['HTTP/1.1 200 OK\r\n',
                                 'Content-Type: audio/mpeg\r\n'
                                 'Connection: Close\r\n'
                                 '\r\n'])
        # Attach transport to the player and forget about the session
        # This has been tested to be safe; twisted still tears down the request
        # when the connection closes.
        sock = request.transport.fileno()
        format = 'audio/mpeg'
        log.msg('New streaming client on %i, requesting %s' %(sock, format))
        self.player.attachFDSink(format, sock)
        return NOT_DONE_YET

def getFactory(musicdir, wsFactory, player):
    # TODO package_resources
    static_path = os.path.join(os.path.dirname(__file__), 'web')
    webdir = File(static_path)
    webdir.putChild('nowplaying', NowPlayingResource(player))
    webdir.putChild('stream', AudioStreamResource(player))
    webdir.putChild('tracks', AJAXTracksResource(musicdir, wsFactory))
    webdir.putChild('ws', WebSocketResource(wsFactory))
    return Site(webdir)
