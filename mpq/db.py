
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship, sessionmaker

engine = create_engine('sqlite:///mpq.sqlite')
Base = declarative_base()
Session = sessionmaker(bind=engine)

from sqlalchemy import Column, Integer, Unicode, String, ForeignKey

class Track(Base):
    """Represents a music file.

    Vote tallying is closely intertwined between :class:Track and :class:Vote.
    Each :class:Track has a :attr:`Track.score` primarly for performance
    reasons, but this requires that a transaction lock the :class:Track while
    modifying the set of corresponding :class:`Vote`s.

    The :class:Track must be locked before any :class:Vote changes are made,
    and well-behaved code should attempt to perfom these operations as fast as
    possible.
    """
    __tablename__ = 'tracks'

    id = Column(Integer, primary_key=True)
    filename = Column(Unicode(length=32), nullable=False, unique=True)

    title = Column(Unicode(length=64))
    artist = Column(Unicode(length=64))
    album = Column(Unicode(length=64))

    score = Column(Integer, nullable=False, default=0)
    votes = relationship("Vote", backref=backref('track'),
                         cascade="save-update, merge, delete")

    def __repr__(self):
        return "<Track('%s')>" %self.filename

    def asCanonical(self):
        out = {}
        for attr in ('title', 'artist', 'album', 'id', 'score'):
            out[attr] = getattr(self, attr)
        return out

class Vote(Base):
    # TODO cocurrency by locking is nasty. Should be able to implement
    # sequencing of votes where each is assigned a strictly increasing sequence
    # number such that modifications can be applied only to votes with sequence
    # numbers indicating they existed when the modification process started.
    __tablename__ = 'votes'

    # Implementation detail: twisted uses a pseudorandom MD5 digest as UID
    uid = Column(String(length=32), nullable=False, primary_key=True)
    track_id = Column(Integer, ForeignKey('tracks.id'), primary_key=True)

    def __repr__(self):
        return '<Vote for "%s" by <%s>>' %(self.track.title, self.uid)

if __name__ == '__main__':
    Base.metadata.create_all(engine)

    import os.path
    from sqlalchemy.orm.exc import NoResultFound
    import audiometa
    print 'Scanning for deleted files'
    s = Session()
    for track in s.query(Track).all():
        if not os.path.exists(os.path.join('music', track.filename)):
            s.delete(track)
	    print 'Removed:', track.filename
    s.commit()

    print 'Scanning for new files'
    for (dirpath, _, files) in os.walk('music'):
        for filename in files:
            filename = unicode(filename)
            filepath = os.path.join(dirpath, filename)
            try:
                t = s.query(Track).filter(Track.filename == filename).one()
            except NoResultFound:
                print 'Added:', filename
            else:
                continue
            meta = audiometa.AudioMeta(filepath)
            track = Track(filename=filename, title=meta['title'],
                          artist=meta['artist'], album=meta['album'])
            s.add(track)
    s.commit()
