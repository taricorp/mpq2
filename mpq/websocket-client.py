import sys
 
from twisted.python import log
from twisted.internet import reactor
 
from autobahn.websocket import connectWS
from autobahn.wamp import WampClientFactory, \
                          WampClientProtocol
 
 
class PubSubClient1(WampClientProtocol):
 
   def onSessionOpen(self):
      self.subscribe("http://mpq.taricorp.net/newtrack", self.onSimpleEvent)
 
   def onSimpleEvent(self, topicUri, event):
      print "Event", topicUri, event
 
if __name__ == '__main__':
 
   log.startLogging(sys.stdout)
   debug = len(sys.argv) > 1 and sys.argv[1] == 'debug'
 
   factory = WampClientFactory("ws://localhost:8001", debugWamp = debug)
   factory.protocol = PubSubClient1
 
   connectWS(factory)
 
   reactor.run()

