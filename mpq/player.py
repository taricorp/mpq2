from __future__ import print_function
import os, sys, time, threading
from sqlalchemy import func, desc
from sqlalchemy.sql.expression import func
from sqlalchemy.orm.exc import NoResultFound
from . import db

# set gobject threads (used by gst) to yield to python while executing native
# methods
import gobject
gobject.threads_init()
import glib

import pygst
pygst.require('0.10')
import gst
import gst.pbutils

# GStreamer ahoy.
#
# http://www.jejik.com/articles/2007/01/streaming_audio_over_tcp_with_python-gstreamer/
# http://pygstdocs.berlios.de/pygst-tutorial/playbin.html

# Playback is all handled by the Player. Just throw file descriptors at
# it and specify the format to give to it, and away it goes.
#
# GstMultiFdSink / GstMultiSocketSink!
#
# In order to implement this on top of Twisted, we'll need to create pipes
# (or some other kind of fd) to forward data through to twisted at the
# protocol level, which is a bit annoying.

# Master pipeline:
#  playbin (playbin2 for gst-0.10)
#   flags = GST_PLAY_FLAG_AUDIO
#   audio-sink -> tee -> output pads
# Streaming pipeline:
#  encodebin
#   profile = user-specified
#   -> multifdsink
#  multifdsink
#   -> twisted (external)
# Local pipeline:
#  autoaudiosink OR
#  alsasink OR
#  osssink

# GstPlayFlags enumeration
GST_PLAY_FLAG_VIDEO         = (1 << 0)
GST_PLAY_FLAG_AUDIO         = (1 << 1)
GST_PLAY_FLAG_TEXT          = (1 << 2)
GST_PLAY_FLAG_VIS           = (1 << 3)
GST_PLAY_FLAG_SOFT_VOLUME   = (1 << 4)
GST_PLAY_FLAG_NATIVE_AUDIO  = (1 << 5)
GST_PLAY_FLAG_NATIVE_VIDEO  = (1 << 6)
GST_PLAY_FLAG_DOWNLOAD      = (1 << 7)
GST_PLAY_FLAG_BUFFERING     = (1 << 8)
GST_PLAY_FLAG_DEINTERLACE   = (1 << 9)
GST_PLAY_FLAG_SOFT_COLORBALANCE = (1 << 10)

# GstRecoverPolicy enumeration
GST_RECOVER_POLICY_NONE = 0
GST_RECOVER_POLICY_RESYNC_LATEST = 1
GST_RECOVER_POLICY_RESYNC_SOFT_LIMIT = 2
GST_RECOVER_POLICY_RESYNC_KEYFRAME = 3

from sys import platform
WINDOWS = platform == 'win32'

class Player(threading.Thread):
    daemon = True

    def __init__(self, musicdir, stream=True):
        super(Player, self).__init__()
        self.musicdir = musicdir
        if stream and WINDOWS:
            # Reason: multifdsink doesn't exist on Windows
            print("Streaming is not supported on Windows servers. Disabling.")
            stream = False
        self.stream = stream
        self.nowPlayingID = None
        self.nextTrack_callbacks = []

        self.mainLoop = glib.MainLoop()

        # playbin - master pipeline
        self.pipeline = gst.element_factory_make('playbin2')
        # TODO do we need a bin for this? Don't think so.
        self.teeBin = gst.Bin()
        self.tee = gst.element_factory_make('tee')
        self.teeBin.add(self.tee)
        self.teeBin.add_pad(gst.GhostPad('sink', self.tee.get_pad('sink')))
        self.pipeline.set_property('flags', GST_PLAY_FLAG_AUDIO)
        self.pipeline.set_property('video-sink', gst.element_factory_make('fakesink'))
        self.pipeline.set_property('audio-sink', self.teeBin)

        # Connect a null sink so the pipeline can run even without any real
        # outputs.
        # TODO this allows the pipeline to run as fast as possible, which is rather annoying.
        self.attachSink(gst.element_factory_make('fakesink'))

        # Connect signals
        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message', self._handleMessage)
        self.pipeline.connect('about-to-finish', self.nextTrack)

    def _handleMessage(self, bus, message):
        t = message.type
        if t == gst.MESSAGE_EOS:
            self.pipeline.set_state(gst.STATE_PLAYING)
        elif t == gst.MESSAGE_INFO:
            pass
        elif t == gst.MESSAGE_WARNING:
            pass
        elif t == gst.MESSAGE_ERROR:
            err, debug = message.parse_error()
            print('Playback error:', err, debug, file=sys.stderr)
            self.mainLoop.quit()
        elif t == gst.MESSAGE_APPLICATION:
            if message.structure.get_name() == 'prepare-next-track':
                self.nextTrack()
        # else: print debug message

    def callback_track(self, callback):
        """Callbacks are executed from a streaming thread, so should attempt
        to execute as fast as possible."""
        self.nextTrack_callbacks.append(callback)

    def nextTrack(self, pipeline=None):
        s = db.Session()
        # Lock scores and votes while mucking with vote counts to avoid losing
        # or failing to log votes.
        # TODO locking works, but seems inefficient. Isolating this transaction
        # may be a better way to do it (but isn't well-supported by SQLA):
        #s.connection().execution_options(isolation_level='SERIALIZABLE')
        track = None
        while track == None:
            try:
                track = s.query(db.Track). \
                          filter(db.Track.score == s.query(func.max(db.Track.score))). \
                          order_by(func.random()). \
                          limit(1). \
                          with_lockmode('update'). \
                          one()
            except NoResultFound:
                print("No tracks available to play. Retrying again soon.")
                # Keep retrying until there's something to do
                s.rollback()
                time.sleep(10)
        # Track chosen, clear its votes
        track.score = 0
        s.query(db.Vote).filter(db.Vote.track == track).delete()
        s.commit()

        path = os.path.join(self.musicdir, track.filename)
        if WINDOWS:
            path = '/' + path.replace('\\', '/')
        self.pipeline.set_property('uri', 'file://' + path)
        self.nowPlayingID = track.id
        # TODO looks like the pipeline sometimes reinitializes bits of itself
        # (particularly if this is slow?). Should set it to PLAYING if
        # necessary.
        s.close()
        for cb in self.nextTrack_callbacks:
            cb(track.id)

    def attachSink(self, *elements):
        def chainLink(prev, next):
            prev.link(next)
            return next
        # Sink gets placed in a Bin with a queue plugged into it.
        bin = gst.Bin()
        queue = gst.element_factory_make('queue')
        elements = (queue,) + elements
        bin.add_many(*elements)
        reduce(chainLink, elements)
        bin.add_pad(gst.GhostPad('sink', queue.get_pad('sink')))
        # Hook into the master tee
        self.teeBin.add(bin)
        return self.tee.link(bin)

    def createEncoder(self, format):
        encoder = gst.element_factory_make('encodebin')
        # caps format: <type>[,<property>=<value>]
        # Use gst-inspect to figure out properties, or refer to the List of
        # Defined Types in gstreamer documentation.
        profile = gst.pbutils.EncodingAudioProfile(
                gst.Caps('audio/mpeg,rate=44100,channels=2'),
                None, gst.caps_new_any(), 0)
        encoder.set_property('profile', profile)
        return encoder

    def attachStream(self, format, fd):
        if not self.stream:
            return False
        encoder = self.createEncoder(format)
        fdsink = gst.element_factory_make('multifdsink')
        fdsink.set_property('buffers-max', 8)
        fdsink.set_property('buffers-soft-max', 16)
        fdsink.set_property('recover-policy', GST_RECOVER_POLICY_RESYNC_LATEST)
        # multifdsink pukes if FDs are added when in NULL state. Force
        # transition to READY here.
        fdsink.set_state(gst.STATE_READY)
        # Hook 'em up
        self.attachSink(encoder, fdsink)
        fdsink.emit('add', fd)
        return True

    def queryProgress(self):
        try:
            elapsed = self.pipeline.query_position(gst.FORMAT_TIME)[0]
            total = self.pipeline.query_duration(gst.FORMAT_TIME)[0]
            return (elapsed, total)
        except gst.QueryError:
            return (None, None)

    def run(self):
        self.nextTrack()
        self.pipeline.set_state(gst.STATE_PLAYING)
        self.mainLoop.run()

def makeLocalOutput():
    return gst.element_factory_make('autoaudiosink')

def nsToMinutes(ns):
    s, _ = divmod(ns, 1000000000)
    m, s = divmod(s, 60)
    return '{0}:{1:02}'.format(m, s)

if __name__ == '__main__':
    import os.path
    p = Player(os.path.abspath('music'))
    p.daemon = True
    p.attachSink(makeLocalOutput())
    f = open('sink', 'wb')
    p.attachFDSink('audio/mpeg', f.fileno())
    p.start()
    while True:
        time.sleep(1)
        elapsed, total = p.queryProgress()
        if elapsed is not None and total is not None:
            print(nsToMinutes(elapsed), '/', nsToMinutes(total), end='\r')
        sys.stdout.flush()
