
// TODO make this nicer ;)
// http://cgeers.com/2013/05/03/angularjs-file-upload/
function UploaderController($scope) {
    $scope.uploads = [];
    $scope.completeCount = 0;

    $('#uploaderControl').fileupload({
        dataType: 'json',
        add: function (e, data) {
            // singleFileUploads must be set (default)
            var context = {
                name: data.files[0].name,
                progress: 0
            };
            data.context = context;
            $scope.$apply(function () {
                $scope.uploads.push(context);
            });

            data.submit();
        },

        progress: function (e, data) {
            var progress = (data.loaded * 100 / data.total).toFixed(1);
            $scope.$apply(function () {
                data.context.progress = progress;
            });
        },

        done: function (e, data) {
            $scope.$apply(function() {
                $scope.completeCount++;
                $scope.uploads.forEach(function(e, i) {
                    if (data.context == e) {
                        $scope.uploads.splice(i, 1);
                    }
                });
            });
        }
    });

}
$(function() {

    var uploads = $('#uploader');
    $('#uploadTab').click(function () {
        if (uploads.is(':hidden'))
            uploads.slideDown();
        else
            uploads.slideUp();
    });
});
