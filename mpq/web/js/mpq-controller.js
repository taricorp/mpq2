mpq = angular.module('mpq', [])

// trackdb service
mpq.factory('trackdb', ['$http', '$q', function ($http, $q) {
    // TODO interface will eventually return subsets of everything
    var tracks = null;

    return {
        get: function (id) {
            var deferred = $q.defer();
            if (tracks !== null) {
                if (!(id in tracks))
                    deferred.reject('No such track');
                else
                    deferred.resolve(tracks[id]);
            } else {
                $http.get('/tracks').success(function (data) {
                    tracks = [];
                    data.forEach(function (track) {
                        tracks[track.id] = track;
                    });
                    deferred.resolve(tracks[id]);
                }).error(function (error) {
                    deferred.reject('HTTP error: ' + error)
                });
            }
            return deferred.promise;
        }
    };
}]);

// wsPubSub (autobahn) service
// TODO alternate implementation of this system is possible with EventSource
mpq.factory('wsPubSub', ['$q', '$rootScope', function ($q, $scope) {
    var session = null;
    var uri = "ws://" + window.location.host + "/ws";

    var d = $q.defer();
    var connected = d.promise;
    ab.connect(uri,
        function SessionOpened (s) {
            session = s;
            d.resolve();
        },
        function SessionLost (code, reason) {

        });

    return {
        subscribe: function (evt, callback) {
            // Subscribe once the connection resolves
            connected.then(function () {
                session.subscribe('http://mpq.taricorp.net/' + evt, function (uri, data) {
                    $scope.$apply(function () {
                        callback(uri, data);
                    });
                });
            });
        }
    };
}]);

// sec2min filter (seconds to mm:ss)
mpq.filter('sec2min', function() {
    return function (input) {
        var min = Math.floor(input / 60);
        var sec = input % 60;
        return min + ':' + ("0" + sec).slice(-2);
    }
});

function TrackListController($scope, $http, $log, wsPubSub) {
    $scope.tracks = [];
    $scope.sortType = 'artist';
    $scope.sortReverse = false;

    $scope.vote = function vote(track) {
        $http.put('/tracks/' + track.id).success(function (score) {
            track.voted = true;
        }).error(function (message, code) {
            if (code == 429) {
                // Already voted for this
                track.voted = true;
            } else {
                $log.error('Vote failed: ' + message);
            }
        });
    };

    $scope.trackByID = function (tid) {
        var len = $scope.tracks.length;
        for (var i = 0; i < len; i++) {
            if ($scope.tracks[i].id == tid)
                return $scope.tracks[i];
        }
        return null;
    };

    // TODO use the tracksdb? (needs mass-retrieval functions)
    $http.get('/tracks').success(function (data) {
        $scope.tracks = data;
    });

    // A new track was uploaded
    wsPubSub.subscribe('newtrack', function(uri, track) {
        $scope.tracks.push(track);
    });

    // Somebody voted for something
    wsPubSub.subscribe('vote', function (uri, tid) {
        var t = $scope.trackByID(tid)
        if (t !== null)
            t.score++;
    });

    // Now playing a track. Its votes are cleared.
    wsPubSub.subscribe('playing', function(uri, tid) {
        var track = $scope.trackByID(tid);
        track.score = 0;
        track.voted = false;
    });
}

function NowPlayingController($scope, $http, $timeout, trackdb, wsPubSub) {
    $scope.track = {
        title: 'Unknown',
        artist: 'Unknown Artist',
        album: 'Unknown Album'
    };
    $scope.elapsed = 0;
    $scope.length = 0;

    // Update elapsed every second until end of track
    var ticking = false;
    var tickElapsed = function () {
        if ($scope.elapsed < $scope.length) {
            $scope.elapsed++;
            $timeout(tickElapsed, 1000);
        } else {
            $scope.elapsed = 0;
            $scope.length = 0;
            ticking = false;
            fetchTrack();
        }
    };

    // Query API endpoint for what's up now
    var fetchTrack = function () {
        $http.get('/nowplaying').success(function (info) {
            if (info.elapsed !== null && info.length !== null) {
                $scope.elapsed = info.elapsed;
                $scope.length = info.length;

                // Start ticking if it previously stopped (EOS)
                if (!ticking) {
                    ticking = true;
                    $timeout(tickElapsed, 1000);
                }
                // Get track metadata for display
                trackdb.get(info.id).then(function (track) {
                    $scope.track = track;
                }, function (reason) {
                    // Failed :(
                });
            } else {
                // No information available on what's playing. Try again soon.
                $timeout(fetchTrack, 500);
            }
        });
    }
    fetchTrack();
}
