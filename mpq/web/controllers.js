angular.module('MPQDefaultModule', []).filter('default', function() {
    return function(input, def) {
        if (input)
            return input
        else
            return def
    }
});

function TrackListCtrl($scope, $http) {
    $http.get('/tracks').success(function(data) {
        $scope.tracks = data.map(function(t) {
            if (!t.artist)
                t.artist = "Unknown Artist";
            if (!t.album)
                t.album = "Unknown Album";
            return t;
        });
    });
}

