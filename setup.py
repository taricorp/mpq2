from setuptools import setup, find_packages

setup(
    name = "mpq",
    version = "0.2",
    packages = find_packages(),
    install_requires = ['twisted', 'autobahn<0.8', 'mutagen', 'sqlalchemy'],

    author = "Peter Marheine",
    author_email = "peter@taricorp.net",
    description = "A collaborative music playlist manager.",
    license = "BSD",
)
