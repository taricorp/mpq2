## Configuration parameters ##

# Directory that stores music files
music_dir = './music/'

# Whether to play music locally
play_local = True

# Network interface to listen on
iface = '0.0.0.0'

# TCP port to listen on
port = 8000

debug=False

## Do not edit below this line ##
import os
music_dir = os.path.abspath(music_dir)
if not os.path.isdir(music_dir):
    os.mkdir(music_dir)

from twisted.application import internet, service
from twisted.internet.protocol import ServerFactory, Protocol
from twisted.python import log

import mpq

top_service = service.MultiService()

# Database
## Create the database if it doesn't exist (or bail out)

# Modification server (websocket)
ws_factory = mpq.websocket.getFactory(iface, port, debug=debug)

# Player
player = mpq.player.Player(music_dir)
if play_local:
    player.attachSink(mpq.player.makeLocalOutput())
player.callback_track(ws_factory.trackChanged_callback)
player.start()

# Main interface (http)
http_factory = mpq.http.getFactory(music_dir, ws_factory, player)
http_service = internet.TCPServer(port, http_factory)
http_service.setServiceParent(top_service)

application = service.Application('mpq')
top_service.setServiceParent(application)
